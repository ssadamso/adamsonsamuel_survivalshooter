﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldText : MonoBehaviour
{
    public static int gold;


    Text text;


    void Awake()
    {
        text = GetComponent<Text>();
        gold = 0;
    }


    void Update()
    {
        text.text = "" + gold;
    }
}
