﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
    public float wallCooldown = 10f;
    //1 is lightning, 2 is fire, 3 is lazer
    public int Spell = 1;
    public GameObject wall;
    public GameObject Lazer;
    public GameObject Fire;
    public GameObject Lightning;

    float timer;
    float wallTimer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    Vector3 wallPosition;
    float xPosition;
    float yPosition;
    float zPosition;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        float xPosition = transform.position.x + 5;
        float yPosition = transform.position.y + 1;
        float zPosition = transform.position.z + 5;

        wallPosition = new Vector3(xPosition, yPosition, zPosition);

        timer += Time.deltaTime;

        wallTimer += Time.deltaTime;

        if (Input.GetButton("Z"))
        {
            Spell = 1;
            Lazer.SetActive(false);
            Fire.SetActive(false);
            Lightning.SetActive(true);
        }

        if (Input.GetButton("X"))
        {
            Spell = 2;
            Lazer.SetActive(false);
            Fire.SetActive(true);
            Lightning.SetActive(false);
        }

        if (Input.GetButton("C"))
        {
            Spell = 3;
            Lazer.SetActive(true);
            Fire.SetActive(false);
            Lightning.SetActive(false);
        }

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            if(Spell == 1)
            {
                damagePerShot = 20;
                timeBetweenBullets = 0.15f;
                range = 100f;
                Shoot();
            }

            if(Spell == 2)
            {
                damagePerShot = 50;
                timeBetweenBullets = 0.30f;
                range = 80f;
                Shoot();
            }

            if(Spell == 3)
            {
                damagePerShot = 5;
                timeBetweenBullets = 0.03f;
                range = 150f;
                Shoot();
            }
        }

        if (Input.GetButton("Fire2") && wallTimer >= wallCooldown)
        {
            wallTimer = 0f;

            Instantiate(wall, wallPosition, transform.rotation);
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}
