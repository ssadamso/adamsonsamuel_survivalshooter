﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWall : MonoBehaviour
{
    public float wallLife = 5f;

    float destroy = 0f;

    private void Update()
    {
        destroy += Time.deltaTime;

        if(destroy >= wallLife)
        {
            Destroy(gameObject, 0f);
        }
    }
}
